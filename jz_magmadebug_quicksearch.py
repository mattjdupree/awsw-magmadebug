import renpy
from renpy.display import im
from modloader import modinfo
import jz_magmalink as ml

try:
    import musicviewer_four_metalookup as musicviewer
    has_music_viewer = True
except ImportError:
    has_music_viewer = False

SOUND_FORMATS = {".mp2", ".mp3", ".opus", ".ogg", ".wav", ".flac", ".oga"}

def limit_size(array):
    for i, v in enumerate(array):
        if i == 60:
            yield "{color=#ffff00}...{/color}"
            break
        yield v

def search(array, needle):
    if needle[0] == "\"" and needle[-1] == "\"":
        needle = needle.strip("\"")
        return list(limit_size(v for v in array if needle == v.lower()))
    if needle[0] == "\"":
        needle = needle.strip("\"")
        return list(limit_size(v for v in array if v.lower().startswith(needle)))
    if needle[-1] == "\"":
        needle = needle.strip("\"")
        return list(limit_size(v for v in array if v.lower().endswith(needle)))
    return list(limit_size(v for v in array if needle in v.lower()))

class QuickSearch:
    def __init__(self, store):
        self.store = store
        self.characters = []
        self.char_lookup = {}
        self.char_image_list = []
        self.image_list = []
        self.music_list = []
        self.sound_list = []
        
        self._last_image = None
        
        store["update"] = self.update
        store["search"] = self.search
        self.store["content"] = ("", "", "", "", "", "")
    
    def update(self):
        self.characters[:] = []
        self.char_lookup = {}
        self.char_image_list[:] = []
        self.image_list[:] = []
        self.music_list[:] = []
        self.sound_list[:] = []
        
        for k, v in renpy.python.store_modules["store"].__dict__.iteritems():
            if isinstance(v, renpy.character.ADVCharacter):
                ch_name = str(k) if v.name is None else str(v.name)
                ch_name += "" if v.image_tag is None else " (" + str(v.image_tag) + ")"
                ch_name += "" if v.name is None else " [" + str(k) + "]"
                self.characters.append(ch_name)
                self.char_lookup[ch_name] = k
        
        for k in renpy.display.image.images.keys():
            if k[0] in self.char_lookup:
                self.char_image_list.append(" ".join(k))
            else:
                self.image_list.append(" ".join(k))
        
        for file_name in renpy.exports.list_files():
            try:
                file_name = str(file_name)
            except UnicodeEncodeError:
                continue
            
            if file_name.startswith("mx/"):
                sound_path = file_name
            elif file_name.startswith("fx/"):
                sound_path = file_name
            elif "/mx/" in file_name:
                i1 = file_name.index("/mx/")
                sound_path = file_name[i1+1:]
            elif "/fx/" in file_name:
                i1 = file_name.index("/fx/")
                sound_path = file_name[i1+1:]
            else:
                continue
            
            i2 = sound_path.rfind(".")
            if i2 > -1 and sound_path[i2:] in SOUND_FORMATS:
                if sound_path.startswith("mx/"):
                    self.music_list.append(sound_path)
                else:
                    self.sound_list.append(sound_path)
        
        self.characters.sort()
        self.char_image_list.sort()
        self.image_list.sort()
        self.music_list.sort()
        self.sound_list.sort()
    
    def search(self, needle):
        if not needle:
            self.store["content"] = ("", "", "", "", "", "")
            self.hide_image()
        elif needle[-1] == "F":
            renpy.display.screen.hide_screen("mdbg_quick_search")
            self.store["content"] = ("", "", "", "", "", "")
            self.hide_image()
        else:
            needle = str(needle).lower()
        
            characters = search(self.characters, needle)
            char_images = search(self.char_image_list, needle)
            images = search(self.image_list, needle)
            sound = search(self.sound_list, needle)
            music = search(self.music_list, needle)
            
            selected_text = ""
            
            if characters:
                char = characters[0]
                characters[0] = "{b}" + characters[0] + "{/b}"
                
                var_name = self.char_lookup[char]
                obj = ml.context[var_name]
                selected_text += "{color=#ffff00}Type:{/color} Character\n"
                selected_text += "{color=#ffff00}Name:{/color} " + str(obj.name) + "\n"
                selected_text += "{color=#ffff00}Variable name:{/color} " + var_name + "\n"
                selected_text += "{color=#ffff00}Image tag:{/color} " + str(obj.image_tag) + "\n"
                selected_text += "{color=#ffff00}Dynamic:{/color} " + str(obj.dynamic) + "\n"
                
                if char in char_images:
                    self.parse_image(char)
                elif char + " normal" in char_images:
                    self.parse_image(char + " normal")
                else:
                    self.hide_image()
            elif char_images:
                selected_text += "{color=#ffff00}Type:{/color} Character sprite\n"
                selected_text += self.parse_image(char_images[0])
                char_images[0] = "{b}" + char_images[0] + "{/b}"
            elif images:
                selected_text += "{color=#ffff00}Type:{/color} Image\n"
                selected_text += self.parse_image(images[0])
                images[0] = "{b}" + images[0] + "{/b}"
            elif sound:
                selected_text += "{color=#ffff00}Type:{/color} Sound\n"
                selected_text += self.parse_sound(sound[0])
                self.hide_image()
                sound[0] = "{b}" + sound[0] + "{/b}"
            elif music:
                selected_text += "{color=#ffff00}Type:{/color} Music\n"
                selected_text += self.parse_sound(music[0])
                
                if has_music_viewer and musicviewer.is_supported(music[0]):
                    meta = musicviewer.metalookup(music[0])
                    if meta:
                        selected_text += "{color=#ffff00}Metadata:{/color}\n"
                        for k, v in meta.iteritems():
                            selected_text += "    {color=#ffff00}" + k.capitalize() + ":{/color} " + str(v) + "\n"
                
                self.hide_image()
                music[0] = "{b}" + music[0] + "{/b}"
            else:
                self.hide_image()
            
            self.store["content"] = (
                "{color=#ffff00}Characters:{/color}\n\n" + "\n".join(characters),
                "{color=#ffff00}Character sprites:{/color}\n\n" + "\n".join(char_images),
                "{color=#ffff00}Other images:{/color}\n\n" + "\n".join(images),
                "{color=#ffff00}Sound:{/color}\n\n" + "\n".join(sound),
                "{color=#ffff00}Music:{/color}\n\n" + "\n".join(music),
                selected_text
            )
        
        renpy.exports.restart_interaction()
    
    def parse_image(self, image_name):
        text = "{color=#ffff00}Name:{/color} " + image_name + "\n"
        
        obj1 = renpy.display.image.images[tuple(image_name.split())]
        obj2 = obj1._target()
        
        text += "{color=#ffff00}Image type:{/color} " + type(obj1).__name__ + "\n"
        
        if not isinstance(obj2, im.ImageBase):
            self.hide_image()
            return text
        
        text += "{color=#ffff00}File path:{/color} " + str(getattr(obj2, "filename", None)) + "\n"
        
        width, height = im.cache.get(obj2).get_size()
        
        text += "{color=#ffff00}Width:{/color} " + str(width) + "\n"
        text += "{color=#ffff00}Height:{/color} " + str(height) + "\n"
        
        if image_name != self._last_image:
            self.show_image(obj2, width, height)
            self._last_image = image_name
        
        return text
    
    def parse_sound(self, sound_path):
        text = "{color=#ffff00}Name:{/color} " + sound_path[sound_path.rfind("/")+1:] + "\n"
        text += "{color=#ffff00}File path:{/color} " + sound_path + "\n"
        
        return text
    
    def show_image(self, img, width, height):
        if width > height:
            new_height = int(465.0 * height / width)
            img = im.Scale(img, 465, new_height)
            pos = (1455, 80 + (465 - new_height) // 2)
        else:
            new_width = int(465.0 * width / height)
            img = im.Scale(img, new_width, 465)
            pos = (1455 + (465 - new_width) // 2, 80)
            
        renpy.exports.show("_mdbg_qs_img", layer="screens", at_list=[ml.context["Position"](xpos=pos[0]/1920.0, ypos=pos[1]/1080.0, xanchor=0, yanchor=0)], what=img)
    
    def hide_image(self):
        if self._last_image != None:
            renpy.exports.hide("_mdbg_qs_img", layer="screens")
            self._last_image = None
