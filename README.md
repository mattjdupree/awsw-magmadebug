# AwSW - MagmaDebug

MagmaDebug is a mod for the game Angels with Scaly Wings, which adds new debugging tools that provide the mod creators with more information about the inner workings of the game in real time in order to help them locate and solve problems.

### Node Tree Viewer

You can press SHIFT+N in game to bring up a Node Tree Viewer screen, which shows the structure of the node tree at the current point in the game, including hooks created by other mods and where they're pointing to. Also it shows other useful information, such as the current filename and line.

### Developer mode

Having this mod installed will automatically enable RenPy's developer mode, which you can access by pressing SHIFT+D, and other developer tools. It contains many useful things, like the console or variable viewer. More about that in the [Ren'Py Documentation](https://www.renpy.org/doc/html/developer_tools.html)
