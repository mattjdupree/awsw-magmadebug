init python:
    config.developer = True
    config.underlay.append(renpy.Keymap(mdbg_show_node_view = Show("mdbg_node_view")))
    config.underlay.append(renpy.Keymap(mdbg_show_quick_search = Show("mdbg_quick_search")))
    config.keymap["mdbg_show_node_view"] = ["shift_K_n"]
    config.keymap["mdbg_show_quick_search"] = ["shift_K_f"]

init 999 python:
    mdbg.quick_search.update()

screen mdbg_node_view:
    key "shift_K_n" action Hide("mdbg_node_view")
    key "shift_K_s" action Function(mdbg.node_view.select, 1)
    key "alt_shift_K_s" action Function(mdbg.node_view.select, -1)
    key "alt_shift_K_x" action Function(mdbg.node_view.set_next)
    timer 0.2 repeat True action Function(mdbg.node_view.refresh)
    
    hbox:
        spacing 20
        
        frame:
            style "_console_input"
            xmaximum 1200
            text "[mdbg.node_view.content[0]]" style "_console_input_text"
        
        frame:
            style "_console_input"
            xmaximum 700
            text "[mdbg.node_view.content[1]]" style "_console_input_text"

screen mdbg_quick_search:
    vbox:
        spacing 20
        
        frame:
            style "_console_input"
            xmaximum 1920
            input changed mdbg.quick_search.search
        
        hbox:
            spacing 10
            
            frame:
                style "_console_input"
                xmaximum 281
                text "[mdbg.quick_search.content[0]]" style "_console_input_text"
            
            frame:
                style "_console_input"
                xmaximum 281
                text "[mdbg.quick_search.content[1]]" style "_console_input_text"
            
            frame:
                style "_console_input"
                xmaximum 281
                text "[mdbg.quick_search.content[2]]" style "_console_input_text"
            
            frame:
                style "_console_input"
                xmaximum 281
                text "[mdbg.quick_search.content[3]]" style "_console_input_text"
            
            frame:
                style "_console_input"
                xmaximum 281
                text "[mdbg.quick_search.content[4]]" style "_console_input_text"
            
            vbox:
                spacing 10
            
                frame:
                    style "_console_input"
                    xmaximum 465
                    ymaximum 465
                
                frame:
                    style "_console_input"
                    xmaximum 465
                    text "[mdbg.quick_search.content[5]]" style "_console_input_text"
