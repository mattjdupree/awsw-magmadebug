from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

import jz_magmadebug_nodeview as nodeview
import jz_magmadebug_quicksearch as quicksearch
from jz_magmadebug_lint import lint
import jz_magmalink as ml

@loadable_mod
class AWSWMod(Mod):
    name = "MagmaDebug"
    version = "0.1.3-dev"
    author = "Jakzie"
    dependencies = ["MagmaLink", "?Music Viewer"]
    
    def mod_load(self):
        mdbg_store = ml.context.create_store("mdbg")
        mdbg_store['lint'] = lint
        nodeview.NodeView(mdbg_store.create_store("node_view"))
        quicksearch.QuickSearch(mdbg_store.create_store("quick_search"))

        ( ml.overlay.Overlay().add(
            [ "textbutton 'Lint':"
            , "    align (0.2,0.2)"
            , "    action [mdbg.lint, Quit()]"
            ])
            .compile_to("main_menu")
        )
    
    def mod_complete(self):
        pass
