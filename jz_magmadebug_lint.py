import renpy
from renpy.lint import *

def lint():
    """
    The master Ren'Py lint function, that's responsible for staging all of the other checks.

    Modified here to remove argument dependence and BOM output and to capture parse errors.
    """
    renpy.game.lint = True

    print(unicode(renpy.version + " lint report, generated at: " + time.ctime()).encode("utf-8"))

    # This supports check_hide.
    setattr(renpy.lint,'image_prefixes',{})
    # image_prefixes = { }

    for k in renpy.display.image.images:
        renpy.lint.image_prefixes[k[0]] = True

    # Setup the AwSW-specific ingame variables
    setattr(renpy.store, 'c', renpy.store.DynamicCharacter("persistent.player_name", color="#ffffff"))

    # Iterate through every statement in the program, processing
    # them. We sort them in filename, linenumber order.

    all_stmts = [ (i.filename, i.linenumber, i) for i in renpy.game.script.all_stmts ]
    all_stmts.sort()

    # The current count.
    counts = collections.defaultdict(Count)

    # The current language.
    language = None

    menu_count = 0
    screen_count = 0
    image_count = 0

    global report_node

    for _fn, _ln, node in all_stmts:
        if isinstance(node, (renpy.ast.Show, renpy.ast.Scene)):
            precheck_show(node)

    for _fn, _ln, node in all_stmts:

        if common(node):
            continue

        setattr(renpy.lint, 'report_node', node)
        report_node = node

        if isinstance(node, renpy.ast.Image):
            image_count += 1
            check_image(node)

        elif isinstance(node, renpy.ast.Show):
            check_show(node, False)

        elif isinstance(node, renpy.ast.Scene):
            check_show(node, True)

        elif isinstance(node, renpy.ast.Hide):
            check_hide(node)

        elif isinstance(node, renpy.ast.With):
            check_with(node)

        elif isinstance(node, renpy.ast.Say):
            try:
                check_say(node)
            except Exception as e:
                report("Exception occurred while attempting to read Say node. (Contents: %s)", e)
                add("Was there a text tag left open?")

            counts[language].add(node.what)

        elif isinstance(node, renpy.ast.Menu):
            check_menu(node)
            menu_count += 1

        elif isinstance(node, renpy.ast.Jump):
            check_jump(node)

        elif isinstance(node, renpy.ast.Call):
            check_call(node)

        elif isinstance(node, renpy.ast.While):
            check_while(node)

        elif isinstance(node, renpy.ast.If):
            check_if(node)

        elif isinstance(node, renpy.ast.UserStatement):
            check_user(node)

        elif isinstance(node, renpy.ast.Label):
            check_label(node)

        elif isinstance(node, renpy.ast.Translate):
            language = node.language

        elif isinstance(node, renpy.ast.EndTranslate):
            language = None

        elif isinstance(node, renpy.ast.Screen):
            screen_count += 1

        elif isinstance(node, renpy.ast.Define):
            check_define(node, "define")

        elif isinstance(node, renpy.ast.Default):
            check_define(node, "default")

    setattr(renpy.lint, 'report_node', None)
    report_node = None

    check_styles()
    check_filename_encodings()

    for f in renpy.config.lint_hooks:
        f()

    lines = [ ]

    def report_language(language):

        count = counts[language]

        if count.blocks <= 0:
            return

        if language is None:
            s = "The game"
        else:
            s = "The {0} translation".format(language)

        s += """ contains {0} dialogue blocks, containing {1} words
and {2} characters, for an average of {3:.1f} words and {4:.0f}
characters per block. """.format(
            humanize(count.blocks),
            humanize(count.words),
            humanize(count.characters),
            1.0 * count.words / count.blocks,
            1.0 * count.characters / count.blocks)

        lines.append(s)

    print()
    print()
    print("Statistics:")
    print()

    languages = list(counts)
    languages.sort()
    for i in languages:
        report_language(i)

    lines.append("The game contains {0} menus, {1} images, and {2} screens.".format(
        humanize(menu_count), humanize(image_count), humanize(screen_count)))

    for l in lines:
        for ll in textwrap.wrap(l, 78):
            print(ll.encode("utf-8"))

        print()

    print()
    if renpy.config.developer and (renpy.config.original_developer != "auto"):
        print("Remember to set config.developer to False before releasing.")
        print()

    print("Lint is not a substitute for thorough testing. Remember to update Ren'Py")
    print("before releasing. New releases fix bugs and improve compatibility.")

    return False
