import renpy
from renpy import ast
from modloader import modast

def parse_menu_choice(item):
    choice = "\"" + item[0] + "\" if " + item[1]
    if item[1] == "False":
        choice = "{color=#ff0000}" + choice + "{/color}"
                
    return choice

def limit_str(string):
    if not isinstance(string, (unicode, str)):
        string = str(string).replace("{", "{{")
    return (string[:500] + " {color=#ffff00}...{/color}") if len(string) > 500 else string

class NodeView:
    def __init__(self, store):
        self.store = store
        self.previous = None
        self.info = ""
        self.nodes = []
        self.lines = []
        self.prefix = ""
        self.selected = 0
        self.selected_next = 1
        self.line_limit = 50
        
        store["content"] = ".."
        store["refresh"] = self.refresh
        store["select"] = self.change_selected
        store["set_next"] = self.change_next
    
    def add_line(self, line):
        if len(self.lines) < self.line_limit:
            self.lines.append(self.prefix + line)
    
    def add_pref_line(self, node, color, line=""):
        if len(self.lines) < self.line_limit:
            self.nodes.append((node, len(self.lines)))
            self.add_line("{color=#" + color + "}<" + type(node).__name__ + ">{/color}  " + line)
    
    def parse_node(self, node, subnodes=False):
        if hasattr(node, "imspec"):
            imspec = getattr(node, "imspec")
            line = " ".join(imspec[0])
            if imspec[3]:
                line += " at " + " ".join(imspec[3])
            if imspec[6]:
                line += " onlayer " + " ".join(imspec[6])
            self.add_pref_line(node, "00ff00", line)
        elif isinstance(node, ast.With):
            self.add_pref_line(node, "00ff00", node.expr)
        elif isinstance(node, ast.Python):
            self.add_pref_line(node, "00ff00", str(node.code.source).replace("{", "{{"))
        elif isinstance(node, ast.Label):
            self.add_pref_line(node, "00ff00", node.name)
        elif isinstance(node, ast.Jump):
            self.add_pref_line(node, "0000ff", "-->  " + node.target)
        elif isinstance(node, ast.Call):
            self.add_pref_line(node, "0000ff", "-->  " + node.label)
        elif isinstance(node, ast.Return):
            self.add_pref_line(node, "0000ff")
        elif isinstance(node, ast.Translate):
            self.add_pref_line(node, "00ff00", node.identifier)
        elif isinstance(node, ast.If):
            if subnodes:
                self.add_pref_line(node, "0000ff")
                for entry in node.entries:
                    self.add_line("    " + entry[0] + ":")
                    self.parse_nodes(entry[1][0], 3, "        ")
                    self.add_line("        ...")
            else:
                self.add_pref_line(node, "0000ff", ", ".join(entry[0] for entry in node.entries))
        elif isinstance(node, ast.Menu):
            if subnodes:
                self.add_pref_line(node, "0000ff")
                for item in node.items:
                    if item[2] is None:
                        self.add_line("    \"" + item[0] + "\"")
                    else:
                        self.add_line("    " + parse_menu_choice(item) + ":")
                        self.parse_nodes(item[2][0], 3, "        ")
                        self.add_line("        ...")
            else:
                self.add_pref_line(node, "0000ff", ", ".join(parse_menu_choice(item) for item in node.items))
        elif isinstance(node, modast.ASTHook):
            next_node = getattr(node, "expected_next", None)
            if next_node is not None:
                condition = getattr(node, "condition", None)
                if condition is None:
                    cond = ""
                else:
                    cond = "if " + condition + "  "
                if subnodes:
                    self.add_pref_line(node, "ff0000", cond + "-->")
                    self.parse_nodes(next_node, 3, "        ")
                    self.add_line("        ...")
                else:
                    self.add_pref_line(node, "ff0000", cond + "-->")
                    self.parse_nodes(next_node, 1, "    ")
        else:
            try:
                self.add_pref_line(node, "00ff00", node.get_code())
            except Exception:
                self.add_pref_line(node, "00ff00")
    
    def parse_nodes(self, node, count, prefix, subnodes=False):
        self.prefix = prefix
        for i in range(count):
            self.parse_node(node, subnodes)
                
            if isinstance(node, modast.ASTHook):
                node = node.old_next
            else:
                node = node.next
            if node is None:
                break
            if len(self.lines) >= self.line_limit:
                break
            
        self.prefix = ""
    
    def set_content_str(self):
        string1 = self.info
        selected_node, selected_line = self.nodes[self.selected]
        selected_next_line = self.nodes[self.selected_next][1]
        
        for i, line in enumerate(self.lines):
            if i == selected_line:
                line = "{b}" + line + "{/b}"
            if i == selected_next_line:
                line = "{u}" + line + "{/u}"
            string1 += line + "\n"
        
        string2 = ""
        string2 += "{color=#00ff00}[SHIFT+N]{/color} - Toggle Node Tree View\n"
        string2 += "{color=#00ff00}[SHIFT+S]{/color} - Change selected node\n"
        string2 += "{color=#00ff00}[ALT+SHIFT+S]{/color} - Change selected node\n"
        string2 += "{color=#00ff00}[ALT+SHIFT+X]{/color} - Set selected node as next\n"
        string2 += "\n\n{color=#ffff00}Selected node:{/color}\n"
        string2 += "    {color=#ffff00}Type:{/color} " + type(selected_node).__name__ + "\n"
        string2 += "    {color=#ffff00}Filename:{/color}  " + selected_node.filename + "\n"
        string2 += "    {color=#ffff00}Line:{/color}  " + str(selected_node.linenumber) + "\n"
        if isinstance(selected_node, modast.ASTHook):
            string2 += "    {color=#ffff00}Attributes:{/color}\n"
            string2 += "        {color=#ffff00}name:{/color}" + limit_str(selected_node.name) + "\n"
            string2 += "        {color=#ffff00}hook_func:{/color}" + limit_str(selected_node.hook_func) + "\n"
            string2 += "        {color=#ffff00}next:{/color}" + limit_str(selected_node.next) + "\n"
            string2 += "        {color=#ffff00}old_next:{/color}" + limit_str(selected_node.old_next) + "\n"
            if hasattr(selected_node, "expected_next"):
                string2 += "        {color=#ffff00}expected_next:{/color}" + limit_str(selected_node.expected_next) + "\n"
            if hasattr(selected_node, "condition"):
                string2 += "        {color=#ffff00}condition:{/color}" + limit_str(selected_node.condition) + "\n"
        elif hasattr(selected_node, "__slots__"):
            string2 += "    {color=#ffff00}Attributes:{/color}\n"
            for slot in selected_node.__slots__:
                string2 += "        {color=#ffff00}" + slot + ":{/color} " + limit_str(getattr(selected_node, slot, None)) + "\n"
        
        self.store["content"] = (string1, string2)
    
    def change_selected(self, count):
        self.selected += count
        if self.selected >= len(self.nodes):
            self.selected -= len(self.nodes)
        elif self.selected < 0:
            self.selected += len(self.nodes)
        self.set_content_str()
    
    def change_next(self):
        self.selected_next = self.selected
        ast.next_node(self.nodes[self.selected][0])
        self.set_content_str()
    
    def refresh(self):
        context = renpy.game.context()
        current = context.current
        
        if current == self.previous:
            return
        else:
            self.nodes[:] = []
            self.lines[:] = []
            self.selected = 0
            self.selected_next = 1
            
            node = renpy.game.script.lookup(current)
            self.previous = current
            
            self.info = ""
            self.info += "{color=#ffff00}Current file:{/color}  " + node.filename + "\n"
            self.info += "{color=#ffff00}Current line:{/color}  " + str(node.linenumber) + "\n"
            self.info += "{color=#ffff00}Return stack depth:{/color}  " + str(len(context.return_stack)) + "\n"
            self.info += "\n"
            
            self.parse_nodes(node, self.line_limit, "", True)
            self.set_content_str()
